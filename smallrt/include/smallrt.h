#ifndef SMALLPT_SMALLPT_H
#define SMALLPT_SMALLPT_H

#include <stdint.h>

typedef struct {
    uint8_t r;
    uint8_t g;
    uint8_t b;
} Color;

typedef Color *Framebuffer;

void PopulateFramebuffer(Framebuffer fb, uint32_t fbWidth, uint32_t fbHeight, double time);

#endif // SMALLPT_SMALLPT_H
