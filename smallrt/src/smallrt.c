#include <smallrt.h>

#include <math.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>

typedef union {
    double v[3];
    struct {
        double x;
        double y;
        double z;
    };
} vec3_t;

double Clamp(double l, double v, double h)
{
    return l > v ? l : (h < v ? h : v);
}

uint8_t ToColorComponent(double v)
{
    return (uint8_t)(Clamp(0.0, v, 1.0) * UINT8_MAX);
}

Color ToColor(vec3_t const *v)
{
    Color const res = {
        .r = ToColorComponent(v->x),
        .g = ToColorComponent(v->y),
        .b = ToColorComponent(v->z)
    };
    return res;
}

double LengthSquared(vec3_t const *v)
{
    return pow(v->x, 2) + pow(v->y, 2) + pow(v->z, 2);
}

double Length(vec3_t const *v)
{
    return sqrt(LengthSquared(v));
}

vec3_t Normalize(vec3_t const *v)
{
    double const len = Length(v);
    vec3_t const res = {
        .x = v->x / len,
        .y = v->y / len,
        .z = v->z / len
    };
    return res;
}

vec3_t Cross(vec3_t const *a, vec3_t const *b)
{
    vec3_t const res = {
        .x = -a->z * b->y + a->y * b->z,
        .y = -a->x * b->z + a->z * b->x,
        .z = -a->y * b->x + a->x * b->y
    };
    return res;
}

vec3_t Sum(vec3_t const *a, vec3_t const *b)
{
    vec3_t const res = {
        .x = a->x + b->x,
        .y = a->y + b->y,
        .z = a->z + b->z
    };
    return res;
}

vec3_t Sub(vec3_t const *a, vec3_t const *b)
{
    vec3_t const res = {
        .x = a->x - b->x,
        .y = a->y - b->y,
        .z = a->z - b->z
    };
    return res;
}

double Dot(vec3_t const *a, vec3_t const *b)
{
    return a->x * b->x + a->y * b->y + a->z * b->z;
}

vec3_t Mul(vec3_t const *a, double b)
{
    vec3_t const res = {
        .x = a->x * b,
        .y = a->y * b,
        .z = a->z * b
    };
    return res;
}

vec3_t MulVec(vec3_t const *a, vec3_t const *b)
{
    vec3_t const res = {
        .x = a->x * b->x,
        .y = a->y * b->y,
        .z = a->z * b->z
    };
    return res;
}

typedef struct {
    vec3_t origin;
    vec3_t direction;
} ray_t;

typedef enum {
    DIFFUSE,
    REFLECTIVE,
    REFRACTIVE
} MaterialType;

typedef struct {
    vec3_t color;
    MaterialType type;
} material_t;

typedef struct {
    double radius;
    vec3_t position;
    material_t material;
} sphere_t;

typedef struct {
    double distance;
    vec3_t position;
    vec3_t normal;
    vec3_t direction;
    bool inside;

    sphere_t const *sphere;
} intersection_t;

static double const EPSILON = 32 * FLT_EPSILON;

intersection_t GetIntersection(sphere_t const *sphere, ray_t const *ray)
{
    intersection_t res = { .distance = INFINITY };

    double const radiusSquared = pow(sphere->radius, 2);

    vec3_t const op = Sub(&sphere->position, &ray->origin);
    double const b = Dot(&op, &ray->direction);
    double const opSquared = LengthSquared(&op);
    double const dSquared = pow(b, 2) - opSquared + radiusSquared;
    if (dSquared < 0.0) {
        return res;
    }

    double const d = sqrt(dSquared);
    bool const inside = opSquared < radiusSquared;
    double const t = inside ? b + d : b - d;
    if (t < EPSILON) {
        return res;
    }

    vec3_t tmp = Mul(&ray->direction, t);
    res.distance = t;
    res.position = Sum(&ray->origin, &tmp);
    res.normal = Sub(&res.position, &sphere->position);
    res.normal = Normalize(&res.normal);
    res.normal = Mul(&res.normal, (inside ? -1.0 : 1.0));
    res.direction = ray->direction;
    res.inside = inside;
    res.sphere = sphere;

    return res;
}

typedef struct {
    vec3_t x;
    vec3_t y;
    vec3_t z;

    vec3_t origin;
} onb_t;

vec3_t ToWorld(onb_t const *onb, vec3_t const *local)
{
    vec3_t res = {};
    vec3_t tmp = {};

    tmp = Mul(&onb->x, local->x);
    res = Sum(&res, &tmp);

    tmp = Mul(&onb->y, local->y);
    res = Sum(&res, &tmp);

    tmp = Mul(&onb->z, local->z);
    res = Sum(&res, &tmp);

    return res;
}

onb_t GetOnbFromZ(vec3_t const *origin, vec3_t const *z)
{
    static const double alike = 0.999;
    static vec3_t const worldY = { .y = 1.0 };
    static vec3_t const worldX = { .x = 1.0 };

    bool const chooseY = fabs(Dot(z, &worldX)) > alike;

    vec3_t xx = Cross(chooseY ? &worldY : &worldX, z);
    xx = Normalize(&xx);

    vec3_t yy = Cross(z, &xx);
    yy = Normalize(&yy);

    onb_t res = {
        .x = xx,
        .y = yy,
        .z = *z,
        .origin = *origin
    };

    return res;
}

double GetUniform()
{
    return (double)rand() / RAND_MAX; // NOLINT
}

ray_t GetRandomHemisphereSample(onb_t const *onb)
{
    ray_t res = { .origin = onb->origin };

    double const r1 = GetUniform();
    double const r2 = GetUniform();

    double const cosTheta = sqrt(1.0 - r1);
    double const sinTheta = sqrt(1.0 - cosTheta * cosTheta);
    double const phi = 2 * M_PI * r2;
    double const sinPhi = sin(phi);
    double const cosPhi = cos(phi);

    vec3_t const local = {
        .x = cosPhi * sinTheta,
        .y = sinPhi * sinTheta,
        .z = cosTheta
    };

    res.direction = ToWorld(onb, &local);

    return res;
}

typedef struct {
    uint32_t width;
    uint32_t height;

    double dp;
    onb_t onb;
} camera_t;

typedef struct {
    uint32_t width;
    uint32_t height;
} viewport_t;

void ConstructCamera(camera_t *camera, vec3_t const *eye, vec3_t const *gaze, vec3_t const *up, viewport_t viewport,
    double fov)
{
    camera->width = viewport.width;
    camera->height = viewport.height;

    camera->dp = 2.0 * tan(fov / 2.0) / camera->height;

    {
        vec3_t const z = Normalize(gaze);
        vec3_t const cross = Cross(gaze, up);
        vec3_t const x = Normalize(&cross);
        vec3_t const y = Cross(&x, &z);

        camera->onb.x = x;
        camera->onb.y = y;
        camera->onb.z = z;
        camera->onb.origin = *eye;
    }
}

ray_t GetRandomRay(camera_t const *camera, uint32_t px, uint32_t py)
{
    double const dp = GetUniform();
    double const dq = GetUniform();

    double const p = (px - camera->width / 2.0 + dp) * camera->dp;
    double const q = (py - camera->height / 2.0 + dq) * camera->dp;

    vec3_t const local = {
        .x = p,
        .y = q,
        .z = 1.0
    };
    vec3_t const world = ToWorld(&camera->onb, &local);

    ray_t const res = {
        .origin = camera->onb.origin,
        .direction = Normalize(&world)
    };
    return res;
}

typedef struct {
    vec3_t position;
    vec3_t color;
} light_t;

typedef struct {
    sphere_t *spheres;
    uint32_t numSpheres;

    light_t *lights;
    uint32_t numLights;

    camera_t camera;
    vec3_t ambient;
} scene_t;

void InitScene(scene_t *scene, uint32_t fbWidth, uint32_t fbHeight)
{
    {
        vec3_t const ambient = {
            .x = 0.05,
            .y = 0.05,
            .z = 0.05
        };
        scene->ambient = ambient;
    }

    {
        viewport_t const viewport = { .width = fbWidth, .height = fbHeight };

        vec3_t const up = {
            .x = 0.0,
            .y = 1.0,
            .z = 0.0
        };

        vec3_t const gaze = {
            .x = 0.0,
            .y = -0.042612,
            .z = -1.0
        };

        vec3_t const position = {
            .x = 50.0,
            .y = 47.4178,
            .z = 164.6
        };

        double const fov = 60.0 / 180.0 * M_PI;
        ConstructCamera(&scene->camera, &position, &gaze, &up, viewport, fov);
    }

    {
        static uint32_t const NUM_SPHERES = 9U;
        scene->numSpheres = NUM_SPHERES;
    }

    scene->spheres = (sphere_t *)malloc(sizeof(sphere_t) * scene->numSpheres);

    { // Right
        sphere_t sphere = {
            .position = { .x = 1e5 + 1.0, .y = 40.8, .z = 81.6 },
            .material = {
                .color = {.x = 0.75, .y = 0.25, .z = 0.25},
                .type = DIFFUSE
            },
            .radius = 1e5
        };
        scene->spheres[0] = sphere;
    }

    { // Left
        sphere_t sphere = {
            .position = { .x = -1e5 + 99, .y = 40.8, .z = 81.6 },
            .material = {
                .color = { .x = 0.25, .y = 0.25, .z = 0.75 },
                .type = DIFFUSE
            },
            .radius = 1e5
        };
        scene->spheres[1] = sphere;
    }

    { // Back
        sphere_t sphere = {
            .position = { .x = 50.0, .y = 40.8, .z = 1e5 },
            .material = {
                .color = { .x = 0.75, .y = 0.75, .z = 0.75 },
                .type = DIFFUSE
            },
            .radius = 1e5
        };
        scene->spheres[2] = sphere;
    }

    { // Front
        sphere_t sphere = {
            .position = { .x = 50.0, .y = 40.8, .z = -1e5 + 170 },
            .material = {
                .color = { .x = 0.0, .y = 0.0, .z = 0.0 },
                .type = DIFFUSE
            },
            .radius = 1e5
        };
        scene->spheres[3] = sphere;
    }

    { // Bottom
        sphere_t sphere = {
            .position = { .x = 50.0, .y = 1e5, .z = 81.6 },
            .material = {
                .color = { .x = 0.75, .y = 0.75, .z = 0.75 },
                .type = DIFFUSE
            },
            .radius = 1e5
        };
        scene->spheres[4] = sphere;
    }

    { // Top
        sphere_t sphere = {
            .position = { .x = 50.0, .y = -1e5 + 81.6, .z = 81.6 },
            .material = {
                .color = { .x = 0.75, .y = 0.75, .z = 0.75 },
                .type = DIFFUSE
            },
            .radius = 1e5
        };
        scene->spheres[5] = sphere;
    }

    { // Mirror
        sphere_t sphere = {
            .position = { .x = 27.0, .y = 16.5, .z = 47 },
            .material = {
                .color = { .x = 1.0, .y = 1.0, .z = 1.0 },
                .type = REFLECTIVE
            },
            .radius = 16.5
        };
        scene->spheres[6] = sphere;
    }

    { // Glass
        sphere_t sphere = {
            .position = { .x = 73.0, .y = 16.5, .z = 78 },
            .material = {
                .color = { .x = 1.0, .y = 1.0, .z = 1.0 },
                .type = REFRACTIVE
            },
            .radius = 16.5
        };
        scene->spheres[7] = sphere;
    }

    { // Lamp
        sphere_t sphere = {
            .position = { .x = 50.0, .y = 681.6 - 0.27, .z = 81.6 },
            .material = {
                .color = { .x = 1.0, .y = 1.0, .z = 1.0 },
                .type = DIFFUSE
            },
            .radius = 600.0
        };
        scene->spheres[8] = sphere;
    }

    {
        static uint32_t const NUM_LIGHTS = 1;
        scene->numLights = NUM_LIGHTS;
    }

    scene->lights = (light_t *)malloc(sizeof(light_t) * scene->numLights);

    { // Lomp
        sphere_t const *lamp = &scene->spheres[8];
        vec3_t const lightPosition =
        {
            .x = lamp->position.x,
            .y = lamp->position.y - lamp->radius - 15.0,
            .z = lamp->position.z - 5.0
        };
        light_t light = {
            .position = lightPosition,
            .color = { .x = 64.0, .y = 64.0, .z = 64.0 }
        };

        scene->lights[0] = light;
    }
}

void DestructScene(scene_t *scene)
{
    free(scene->spheres);
    free(scene->lights);
}

void UpdateScene(scene_t *scene, double time)
{

}

intersection_t GetNearestIntersection(scene_t const *scene, ray_t const *ray)
{
    intersection_t intersection = { .distance = INFINITY };

    for (uint32_t i = 0U; i < scene->numSpheres; ++i) {
        intersection_t tmp = GetIntersection(&scene->spheres[i], ray);
        if (isfinite(tmp.distance) && tmp.distance < intersection.distance) {
            intersection = tmp;
        }
    }

    return intersection;
}

vec3_t GetIdealReflect(vec3_t const *rayDir, vec3_t const *norm)
{
    vec3_t const dV = Mul(norm, 2.0 * Dot(norm, rayDir));
    vec3_t const V = Sub(rayDir, &dV);
    return Normalize(&V);
}

double GetR0(double iorIn, double iorOut)
{
    double const sqrtR0 = (iorIn - iorOut) / (iorIn + iorOut);
    return sqrtR0 * sqrtR0;
}

double GetReflectivity(vec3_t const *rayDir, vec3_t const *norm, double iorIn, double iorOut)
{
    double const R0 = GetR0(iorIn, iorOut);
    double const cos_i = -1.0 * Dot(rayDir, norm);

    if (iorIn <= iorOut) {
        return R0 + (1.0 - R0) * pow(1.0 - cos_i, 5.0);
    }

    double const iorRatio = iorIn / iorOut;
    double const sin_t2 = iorRatio * iorRatio * (1.0 - cos_i * cos_i);

    bool const TIR = sin_t2 >= 1.0;
    if (TIR) {
        return 1.0;
    }

    double const cos_t = sqrt(1.0 - sin_t2);
    return R0 + (1.0 - R0) * pow(1.0 - cos_t, 5.0);
}

vec3_t GetIdealTransmit(vec3_t const *rayDir, vec3_t const *norm, double iorIn, double iorOut)
{
    double const cos_i = -1.0 * Dot(rayDir, norm);
    double const iorRatio = iorIn / iorOut;
    double const sin_t2 = iorRatio * iorRatio * (1.0 - cos_i * cos_i);

    vec3_t const V = Mul(rayDir, iorRatio);
    vec3_t const dV = Mul(norm, iorRatio * cos_i - sqrt(1.0 - sin_t2));

    vec3_t res = Sum(&V, &dV);
    return Normalize(&res);
}

bool GetNextRays(intersection_t const *intersection, double iorIn, double iorOut, double reflectivity, ray_t rays[2])
{
    material_t const *material = &intersection->sphere->material;
    vec3_t const *position = &intersection->position;
    vec3_t const *normal = &intersection->normal;
    vec3_t const *rayDir = &intersection->direction;

    vec3_t const idealReflectDir = GetIdealReflect(rayDir, normal);
    ray_t const res0 = {
        .origin = *position,
        .direction = idealReflectDir
    };
    rays[0] = res0;

    if (material->type != REFRACTIVE || (reflectivity + EPSILON) >= 1.0) {
        return false;
    }

    vec3_t const idealTransmitDir = GetIdealTransmit(rayDir, normal, iorIn, iorOut);
    ray_t const res1 = {
        .origin = *position,
        .direction = idealTransmitDir
    };
    rays[1] = res1;

    return true;
}

ray_t GetRayFromTwoPoints(vec3_t const *a, vec3_t const *b)
{
    ray_t res = {
        .origin = *a
    };

    res.direction = Sub(b, a);
    res.direction = Normalize(&res.direction);

    return res;
}

bool AccumulateLights(scene_t const *scene, intersection_t const *intersection, vec3_t *color)
{
    bool foundVisibleLight = false;

    for (uint32_t i = 0; i < scene->numLights; ++i) {
        light_t const *light = &scene->lights[i];
        vec3_t const toLight = Sub(&light->position, &intersection->position);
        double const distanceToLightSq = LengthSquared(&toLight);

        ray_t rayToLight = GetRayFromTwoPoints(&intersection->position, &light->position);
        intersection_t nearest = GetNearestIntersection(scene, &rayToLight);

        if (isfinite(nearest.distance) && (nearest.distance * nearest.distance) <= distanceToLightSq) {
            continue;
        }

        foundVisibleLight = true;

        double const invDist = 1.0 / sqrt(distanceToLightSq);
        vec3_t const emittance = Mul(&light->color, invDist);

        double const dot = fabs(Dot(&intersection->normal, &rayToLight.direction));

        vec3_t colorImpact = Mul(&emittance, dot);
        colorImpact = MulVec(&colorImpact, &intersection->sphere->material.color);
        *color = Sum(color, &colorImpact);
    }

    *color = Mul(color, 1.0 / scene->numLights);

    return foundVisibleLight;
}

vec3_t DecideColor(scene_t const *scene, ray_t const *ray, uint32_t depth)
{
    vec3_t color = scene->ambient;

    static uint32_t const NUM_SAMPLES = 5;
    if (depth >= NUM_SAMPLES) {
        return color;
    }

    intersection_t nearest = GetNearestIntersection(scene, ray);

    if (!isfinite(nearest.distance)) {
        return color;
    }

    bool const needNextRays = nearest.sphere->material.type != DIFFUSE;
    if (!needNextRays) {
        bool const foundLights = AccumulateLights(scene, &nearest, &color);
        if (foundLights) {
            return color;
        } else {
            onb_t const onb = GetOnbFromZ(&nearest.position, &nearest.normal);

            for (uint32_t i = 0; i < NUM_SAMPLES; ++i) {
                ray_t sampleRay = GetRandomHemisphereSample(&onb);
                vec3_t tmp = DecideColor(scene, &sampleRay, depth + 1);
                tmp = MulVec(&nearest.sphere->material.color, &tmp);
                color = Sum(&color, &tmp);
            }

            color = Mul(&color, 1.0 / NUM_SAMPLES);
            return color;
        }
    }

    static double const IOR_GLASS = 2.5;
    static double const IOR_AIR = 1.0;

    double iorIn = IOR_AIR;
    double iorOut = IOR_GLASS;
    double reflectivity = 1.0;

    if (nearest.sphere->material.type == REFRACTIVE) {
        if (nearest.inside) {
            iorIn = IOR_GLASS;
            iorOut = IOR_AIR;
        }
        reflectivity = GetReflectivity(&nearest.direction, &nearest.normal, iorIn, iorOut);
    }

    ray_t rays[2] = {};
    bool const both = GetNextRays(&nearest, iorIn, iorOut, reflectivity, rays);

    if (both) {
        vec3_t color0 = DecideColor(scene, &rays[0], depth + 1);
        color0 = Mul(&color0, reflectivity);

        vec3_t color1 = DecideColor(scene, &rays[1], depth + 1);
        color1 = Mul(&color1, 1.0 - reflectivity);

        color = Sum(&color0, &color1);
    } else {
        color = DecideColor(scene, &rays[0], depth + 1);
    }

    return color;
}

void RenderScene(scene_t const *scene, Framebuffer fb)
{
#pragma omp parallel for default(shared) // NOLINT
    for (uint32_t y = 0U; y < scene->camera.height; ++y) {
        for (uint32_t x = 0U; x < scene->camera.width; ++x) {
            ray_t const ray = GetRandomRay(&scene->camera, x, y);
            vec3_t const color = DecideColor(scene, &ray, 0);

            fb[y * scene->camera.width + x] = ToColor(&color);
        }
    }
}

typedef struct {
    bool init;
    scene_t scene;
} context_t;

context_t *g_contextPtr = NULL;

void DestructContext()
{
    if (!g_contextPtr) {
        return;
    }

    DestructScene(&g_contextPtr->scene);

    g_contextPtr->init = false;
}

void InitContext(context_t *context, uint32_t fbWidth, uint32_t fbHeight)
{
    if (context->init) {
        return;
    }
    g_contextPtr = context;

    srand(time(NULL)); // NOLINT

    InitScene(&context->scene, fbWidth, fbHeight);

    context->init = true;
    atexit(DestructContext);
}

void PopulateFramebuffer(Framebuffer fb, uint32_t fbWidth, uint32_t fbHeight, double time)
{
    static context_t context = { .init = false };
    InitContext(&context, fbWidth, fbHeight);

    UpdateScene(&context.scene, time);
    RenderScene(&context.scene, fb);
}
