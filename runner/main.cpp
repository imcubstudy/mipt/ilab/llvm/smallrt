#include <string_view>
#include <renderer.h>

class Runner final {
public:
    Runner() = default;
    ~Runner() = default;

    void Run()
    {
        while (!renderer_.ShouldClose()) {
            renderer_.Render();
        }
    }

private:
    constexpr static auto WINDOW_WIDTH = 480;
    constexpr static auto WINDOW_HEIGHT = 320;
    constexpr static auto WINDOW_TITLE = std::string_view("smallrt runner");

    Renderer renderer_ = Renderer(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE, PopulateFramebuffer);
};

int main([[maybe_unused]] int const argc, [[maybe_unused]] char const *argv[])
{
    auto runner = Runner();
    runner.Run();
    return 0;
}
