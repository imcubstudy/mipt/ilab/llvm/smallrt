#ifndef SMALLPT_RENDERER
#define SMALLPT_RENDERER

#include <string_view>
#include <memory>
#include <chrono>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

extern "C" {
#include <smallrt.h>
};

class Window final {
public:
    [[nodiscard]] Window(int width, int height, std::string_view const &title);
    ~Window();

    [[nodiscard]] bool ShouldClose();
    void SwapBuffers();

private:
    GLFWwindow *window_ = nullptr;

    std::size_t const width_ = 0U;
    std::size_t const height_ = 0U;
};

class Renderer final {
public:
    using PopulateFramebufferCallback = void (*)(Framebuffer, std::uint32_t , std::uint32_t, double);

    Renderer(int width, int height, std::string_view const &title,
         PopulateFramebufferCallback populateFramebufferCallback);
    ~Renderer();

    void Render();
    [[nodiscard]] bool ShouldClose();

private:
    double GetTime();

    Window window_;

    std::size_t const width_ = 0U;
    std::size_t const height_ = 0U;
    std::chrono::steady_clock::time_point const startTime_{};

    PopulateFramebufferCallback fbCallback_ = nullptr;

    static std::string_view const vsSource_;
    static std::string_view const fsSource_;

    GLuint program_ = 0U;

    struct vertex_t
    {
        float position_[2];
        float texCoord_[2];
    };

    static vertex_t const vertices_[4];
    static GLuint const indices_[6];

    GLuint vao_ = 0U;
    GLuint vbo_ = 0U;
    GLuint ibo_ = 0U;

    GLuint pbo_ = 0U;
    GLuint to_ = 0U;
};

#endif // SMALLPT_RENDERER
