#include "renderer.h"

#include <stdexcept>
#include <cstddef>
#include <algorithm>
#include <cstring>
#include <string>

Window::Window(int width, int height, std::string_view const &title)
    : width_(width),
      height_(height)
{
    glfwSetErrorCallback([](int code, char const *message) {
        std::printf("GLFW error [%d]: %s\n", code, message);
        std::fflush(stdout);
    });

    if (GLFW_TRUE != glfwInit()) {
        throw std::runtime_error("Failed to init GLFW");
    }

    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    window_ = glfwCreateWindow(width, height, title.data(), nullptr, nullptr);
    if (!window_) {
        throw std::runtime_error("Failed to create GLFW window");
    }

    glfwMakeContextCurrent(window_);
    if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) {
        throw std::runtime_error("Failed to load proc addresses");
    }

    struct {
        GLint major_ = 0;
        GLint minor_ = 0;
        char const *vendor_ = nullptr;
        char const *renderer_ = nullptr;
        char const *glslVersion_ = nullptr;
    } glVersion;

    glGetIntegerv(GL_MAJOR_VERSION, &glVersion.major_);
    glGetIntegerv(GL_MINOR_VERSION, &glVersion.minor_);
    glVersion.vendor_ = reinterpret_cast<char const *>(glGetString(GL_VENDOR));
    glVersion.renderer_ = reinterpret_cast<char const *>(glGetString(GL_RENDERER));
    glVersion.glslVersion_ = reinterpret_cast<char const *>(glGetString(GL_SHADING_LANGUAGE_VERSION));

    std::printf("OpenGL info\nVendor: %s\nRenderer: %s\nVersion: %d.%d\nGLSL: %s\n", glVersion.vendor_,
        glVersion.renderer_, glVersion.major_, glVersion.minor_, glVersion.glslVersion_);

    if (glVersion.minor_ >= 3) {
        glDebugMessageCallback([](GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
            GLchar const *message, void const *userParam) {
                if (severity == GL_DEBUG_SEVERITY_HIGH) {
                    throw std::runtime_error(message);
                }
            }, nullptr);
    }
}

Window::~Window()
{
    if (window_) {
        glfwDestroyWindow(window_);
    }

    glfwTerminate();
}

bool Window::ShouldClose()
{
    return glfwWindowShouldClose(window_);
}

void Window::SwapBuffers()
{
    glfwSwapBuffers(window_);
    glfwPollEvents();
}

std::string_view const Renderer::vsSource_ = R"glsl(
#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texCoord;

out VS_OUT
{
    vec2 texCoord;
} vs_out;

void main()
{
    vs_out.texCoord = texCoord;
    gl_Position = vec4(position.x, position.y, 0.0, 1.0);
}

)glsl";

std::string_view const Renderer::fsSource_ = R"glsl(
#version 330 core

out vec4 FragColor;

in VS_OUT
{
    vec2 texCoord;
} fs_in;

uniform sampler2D u_texture;

void main()
{
   FragColor = texture(u_texture, fs_in.texCoord);
}

)glsl";

Renderer::vertex_t const Renderer::vertices_[4] = {
    // Bottom left
    Renderer::vertex_t {
        { -1.0, -1.0 },
        {  0.0,  0.0 }
    },
    // Bottom right
    Renderer::vertex_t {
        {  1.0, -1.0 },
        {  1.0,  0.0 }
    },
    // Top left
    Renderer::vertex_t {
        { -1.0,  1.0 },
        {  0.0,  1.0 }
    },
    // Top right
    Renderer::vertex_t {
        {  1.0,  1.0 },
        {  1.0,  1.0 }
    }
};

GLuint const Renderer::indices_[6] = {
    0, 1, 2,
    1, 3, 2
};

Renderer::Renderer(int width, int height, std::string_view const &title,
   PopulateFramebufferCallback populateFramebufferCallback)
    : window_(width, height, title),
      width_(width),
      height_(height),
      startTime_(std::chrono::steady_clock::now()),
      fbCallback_(populateFramebufferCallback)
{
    auto compileShader = [](std::string_view const &src, GLenum type) -> GLuint {
        GLuint id = glCreateShader(type);
        char const *src_data = src.data();
        glShaderSource(id, 1, &src_data, nullptr);
        glCompileShader(id);

        GLint compileStatus = 0;
        glGetShaderiv(id, GL_COMPILE_STATUS, &compileStatus);
        if (GL_TRUE != compileStatus) {
            auto constexpr errorLength = 512;
            auto compileError = std::make_unique<char[]>(errorLength);
            glGetShaderInfoLog(id, errorLength, nullptr, compileError.get());
            throw std::runtime_error(std::string(compileError.get()));
        }
        return id;
    };

    GLuint vs = compileShader(vsSource_, GL_VERTEX_SHADER);
    GLuint fs = compileShader(fsSource_, GL_FRAGMENT_SHADER);

    program_ = glCreateProgram();
    glAttachShader(program_, vs);
    glAttachShader(program_, fs);
    glLinkProgram(program_);

    GLint linkStatus = 0;
    glGetProgramiv(program_, GL_LINK_STATUS, &linkStatus);
    if(GL_TRUE != linkStatus) {
        auto constexpr errorLength = 512;
        auto linkError = std::make_unique<char[]>(errorLength);
        glGetProgramInfoLog(program_, errorLength, nullptr, linkError.get());
        throw std::runtime_error(std::string(linkError.get()));
    }

    glDeleteShader(vs);
    glDeleteShader(fs);

    glUseProgram(program_);

    glGenVertexArrays(1, &vao_);
    glBindVertexArray(vao_);

    glGenBuffers(1, &vbo_);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_), vertices_, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_t),
        reinterpret_cast<void *>(offsetof(vertex_t, position_)));
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_t),
        reinterpret_cast<void *>(offsetof(vertex_t, texCoord_)));
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &ibo_);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices_), indices_, GL_STATIC_DRAW);

    glClearColor(0.0F, 0.0F, 0.0F, 0.0F);

    glGenTextures(1, &to_);
    glBindTexture(GL_TEXTURE_2D, to_);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, static_cast<GLsizei>(width_), static_cast<GLsizei>(height_), 0,
        GL_RGB, GL_UNSIGNED_BYTE, nullptr);

    glUniform1i(glGetUniformLocation(program_, "u_texture"), 0);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, to_);


    glGenBuffers(1, &pbo_);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo_);
    glBufferData(GL_PIXEL_UNPACK_BUFFER, static_cast<GLsizeiptr>(width_ * height_ * sizeof(Color)), nullptr,
        GL_STREAM_DRAW);

    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo_);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, static_cast<GLsizei>(width_), static_cast<GLsizei>(height_),
        GL_RGB, GL_UNSIGNED_BYTE, nullptr);
}

Renderer::~Renderer()
{
    glDeleteProgram(program_);
    glDeleteBuffers(1, &ibo_);
    glDeleteBuffers(1, &vbo_);
    glDeleteBuffers(1, &pbo_);
    glDeleteTextures(1, &to_);
}

void Renderer::Render()
{
    auto *pixelData = reinterpret_cast<Color *>(glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY));
    fbCallback_(pixelData, width_, height_, GetTime());
    glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, static_cast<GLsizei>(width_), static_cast<GLsizei>(height_),
        GL_RGB, GL_UNSIGNED_BYTE, nullptr);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

    window_.SwapBuffers();
}

bool Renderer::ShouldClose()
{
    return window_.ShouldClose();
}

double Renderer::GetTime()
{
    auto now = std::chrono::steady_clock::now();
    auto duration = now - startTime_;
    double timeMicroseconds = std::chrono::duration_cast<std::chrono::microseconds>(duration).count();
    double microToReg = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::seconds(1)).count();
    return timeMicroseconds / microToReg;
}
